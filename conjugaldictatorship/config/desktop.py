from frappe import _

def get_data():
	return [
		{
			"module_name": "Conjugal Dictatorship",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Conjugal Dictatorship")
		}
	]
