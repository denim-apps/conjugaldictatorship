import json
import frappe

__version__ = '0.0.1'

def is_chapter_unlocked(index):
  values = json.loads(frappe.db.get_value('Web Page Block', [
    ['web_template', '=', 'Chapter Selector'],
    ['web_template_values', 'LIKE', '{"chapter_number":' + str(index) + ',%']
  ], 'web_template_values') or '{}')

  if not values.get('locked'):
    return True

  if not values.get('questions') or not len(values.get('questions')):
    if is_chapter_unlocked(index - 1):
      unlock_chapter(index)
      return True

  if not frappe.db.exists('Subscriber', frappe.session.user):
    return False

  subscriber = frappe.get_doc('Subscriber', frappe.session.user)

  for chapter in subscriber.unlocked_chapters:
    if chapter.chapter == index:
      return True

  return False

def unlock_chapter(index):
  if not frappe.db.exists('Subscriber', frappe.session.user):
    return

  subscriber = frappe.get_doc('Subscriber', frappe.session.user)
  has_chapter_unlocked = False

  for chapter in subscriber.unlocked_chapters:
    if chapter.chapter == index:
      has_chapter_unlocked = True

  if not has_chapter_unlocked:
    subscriber.append('unlocked_chapters', {
      'chapter': index,
    })
    subscriber.save(ignore_permissions=True)
