import frappe

def execute():
  frappe.reload_doc('Conjugal Dictatorship', 'DocType', 'Book Order')
  frappe.db.sql('''
    UPDATE `tabBook Order`
    SET `placed` = 1
    WHERE
      (`first_name` <> '' AND `first_name` IS NOT NULL)
      OR `paid` = 1
  ''')
