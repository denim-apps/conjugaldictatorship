from conjugaldictatorship.conjugal_dictatorship import upsert_order
import frappe

def on_render(template, values):
  values.items = []
  values.totals = {
    'subtotal': 0,
    'shipping': 0,
    'discount': 0,
    'total': 0,
  }

  if values.source == 'Order':
    order = frappe.get_doc('Book Order', frappe.form_dict.get('order'))
  else:
    order = upsert_order(frappe.request.cookies.get('order_number'))

    if frappe.local.form_dict and frappe.local.form_dict.get('remove_product'):
      for item in order.items:
        if item.type == frappe.local.form_dict.get('remove_product'):
          order.items.remove(item)
          order.save(ignore_permissions=True)

    if frappe.local.form_dict and frappe.local.form_dict.get('binding') and frappe.local.form_dict.get('quantity'):
      binding = frappe.local.form_dict.get('binding')
      quantity = frappe.local.form_dict.get('quantity')

      # Check if the order has this binding as an item.
      has_item = False

      for item in order.items:
        if item.type == binding:
          has_item = True
          item.quantity += float(quantity)

      if not has_item:
        order.append('items', {
          'type': binding,
          'quantity': float(quantity),
        })

      order.save(ignore_permissions=True)
      frappe.local.cookie_manager.set_cookie('order_number', order.name)

  for item in order.items:
    item.type_obj = frappe.get_doc('Book Type', item.type)

  values.items = order.items
  values.totals['subtotal'] = order.subtotal
  values.totals['shipping'] = order.shipping
  values.totals['discount'] = order.discount
  values.totals['total'] = order.total
