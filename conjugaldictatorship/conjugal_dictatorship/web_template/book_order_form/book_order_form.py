import frappe

def on_render(template, values):
  settings = frappe.get_doc('Book Settings')
  values.types = list(map(lambda x: x.get_valid_dict(convert_dates_to_str=True), (type for type in settings.types if not type.out_of_stock)))
  values.discounts = list(map(lambda x: x.get_valid_dict(convert_dates_to_str=True), settings.discounts))
