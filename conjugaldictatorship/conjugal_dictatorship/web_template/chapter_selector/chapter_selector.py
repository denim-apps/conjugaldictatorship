import json
from re import M
from conjugaldictatorship import is_chapter_unlocked, unlock_chapter
import frappe
import random

def on_render(template, values):
  if values.locked and not is_chapter_unlocked(values.chapter_number):
    raise frappe.PermissionError('You do not have access to this chapter.')

  previous_chapter = frappe.db.get_value('Web Page Block', [
    ['web_template', '=', 'Chapter Selector'],
    ['web_template_values', 'LIKE', '{"chapter_number":' + str(values.chapter_number - 1) + ',%']
  ], 'parent')
  next_chapter = frappe.db.get_value('Web Page Block', [
    ['web_template', '=', 'Chapter Selector'],
    ['web_template_values', 'LIKE', '{"chapter_number":' + str(values.chapter_number + 1) + ',%']
  ], 'parent')
  previous_chapter = frappe.get_doc('Web Page', previous_chapter) if previous_chapter else None
  next_chapter = frappe.get_doc('Web Page', next_chapter) if next_chapter else None

  if previous_chapter:
    values.previous_chapter_url = frappe.utils.get_url(previous_chapter.route)

  if next_chapter:
    values.next_chapter_url = frappe.utils.get_url(next_chapter.route)

  values.can_read_prev = frappe.session.user != 'Guest'

  # Check if the previous chapter can be read.
  values.can_read_prev = values.can_read_prev and is_chapter_unlocked(values.chapter_number - 1)

  values.can_read_next = frappe.session.user != 'Guest'

  # Check if the next chapter can be read.
  values.can_read_next = values.can_read_next and is_chapter_unlocked(values.chapter_number + 1)
  values.answer_results = []
  values.quiz_answered = frappe.form_dict.get('q_a') == 'yes'
  values.answers_dict = frappe.form_dict
  values.all_correct = True

  if values.questions:
    for i, q in enumerate(values.questions):
      q['answers'] = [q['correct_answer'], q['incorrect_answer_1']]

      if q['incorrect_answer_2']:
        q['answers'].append(q['incorrect_answer_2'])

      if q['incorrect_answer_3']:
        q['answers'].append(q['incorrect_answer_3'])

      random.shuffle(q['answers'])
      
      if values.quiz_answered:
        result = q['correct_answer'] == frappe.form_dict.get('q_' + str(i))
        values.answer_results.append(result)
        values.all_correct = values.all_correct and result

  if values.quiz_answered and values.all_correct:
    values.can_read_next = True
    unlock_chapter(values.chapter_number + 1)

  all_chapters = frappe.db.sql('SELECT * FROM `tabWeb Page Block` WHERE web_template = "Chapter Selector" ORDER BY CAST(JSON_EXTRACT(web_template_values, "$.chapter_number") AS UNSIGNED) ASC', as_dict=True)
  unlocked_chapters = []

  for chapter in all_chapters:
    template_values = json.loads(chapter.web_template_values)
  
    if not template_values.get('locked') or is_chapter_unlocked(template_values.get('chapter_number')):
      page = frappe.get_doc('Web Page', chapter.parent)
      page.url = frappe.utils.get_url(page.route)
      page.chapter_number = template_values.get('chapter_number')
      unlocked_chapters.append(page)

  values.unlocked_chapters = unlocked_chapters
