import frappe

def on_render(template, values):
  settings = frappe.get_doc('Book Settings')
  order = frappe.get_doc('Book Order', frappe.form_dict.order)
  
  if not order.paid:
    values.order = order
    values.settings = settings
  else:
    frappe.throw('Invalid order')
