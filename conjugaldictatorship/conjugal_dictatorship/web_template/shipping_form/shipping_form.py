from conjugaldictatorship.conjugal_dictatorship import upsert_order, shipping_fields, subscribe_fields
import frappe
from frappe import utils

def on_render(template, values):
  settings = frappe.get_doc('Book Settings')
  order_meta = frappe.get_meta('Book Order')
  fields = []

  for field in order_meta.fields:
    if field.fieldname in (subscribe_fields if values.subscribe_form else shipping_fields):
      fields.append(field.get_valid_dict(convert_dates_to_str=True))

  values.shipping_fields = fields

  order = upsert_order(frappe.request.cookies.get('order_number'))

  total_quantity = 0
  self_pickup = False

  for item in order.items:
    item.type_obj = frappe.get_doc('Book Type', item.type)
    total_quantity += item.quantity

    if item.type_obj.pickup_only:
      self_pickup = True

  values.self_pickup = self_pickup
  values.items = order.items
  values.totals = { }
  values.totals['subtotal'] = order.subtotal
  values.totals['shipping'] = order.shipping
  values.totals['discount'] = order.discount
  values.totals['total'] = order.total
  values.total_without_shipping = order.total - order.shipping
  values.total_quantity = total_quantity
  values.order = order.get_valid_dict(convert_dates_to_str=True)
