import json
import frappe
from frappe import utils
from frappe.utils import csvutils

from frappe.desk.search import search_widget, build_for_autosuggest
import requests
from werkzeug import Response

shipping_fields = [
  'first_name',
  'last_name',
  'email',
  'phone_number',
  'date_of_birth',
  'country',
  'street_address',
  'barangay',
  'city',
  'province',
  'postal_code',
  'payment_method',
  'create_an_account',
]

subscribe_fields = [
  'first_name',
  'last_name',
  'email',
  'phone_number',
  'date_of_birth',
  'country',
  'barangay',
  'city',
  'province',
  'postal_code',
]

def upsert_order(order_number):
  order = frappe.new_doc('Book Order')
  order.items = []
  order.calculate()

  if frappe.db.exists('Book Order', order_number):
    new_order = frappe.get_doc('Book Order', order_number)

    if not new_order.paid:
      order = new_order

  if not order.email and frappe.session.user and frappe.db.exists('Subscriber', frappe.session.user):
    subscriber = frappe.get_doc('Subscriber', frappe.session.user)

    for field in shipping_fields:
      if subscriber.get(field):
        order.set(field, subscriber.get(field))

    order.subscriber = subscriber.name
    order.save(ignore_permissions=True)

  return order

@frappe.whitelist(allow_guest=True)
def search_link(doctype, txt, query=None, filters=None, page_length=20, searchfield=None, reference_doctype=None, ignore_user_permissions=False):
  search_widget(doctype, txt.strip(), query, searchfield=searchfield, page_length=page_length, filters=filters, reference_doctype=reference_doctype, ignore_user_permissions=ignore_user_permissions)
  frappe.response['results'] = build_for_autosuggest(frappe.response["values"])
  del frappe.response["values"]

@frappe.whitelist(allow_guest=True)
def checkout():
  if frappe.request.cookies.get('order_number'):
    order = frappe.get_doc('Book Order', frappe.request.cookies.get('order_number'))
  else:
    frappe.throw('No order')

  if order.paid:
    frappe.throw('No order')

  for field in shipping_fields:
    if field in frappe.form_dict:
      order.set(field, frappe.form_dict.get(field))

  order.save(ignore_permissions=True)
  order.set('placed', True)
  order.save(ignore_permissions=True)

  # Save their details
  frappe.db.commit()

  if (order.create_an_account == True or order.create_an_account == '1') and not order.subscriber:
    subscriber = frappe.new_doc('Subscriber')
    subscriber.update(order.get_valid_dict())
    subscriber.save(ignore_permissions=True)
    order.subscriber = subscriber.name
    order.save(ignore_permissions=True)

  frappe.local.cookie_manager.set_cookie('order_number', '')
  return '/pay/' + order.name

PP_SANDBOX_BASE_URL = 'https://api-m.sandbox.paypal.com'
PP_BASE_URL = 'https://api-m.paypal.com'

def pp_base_url():
  settings = frappe.get_doc('Book Settings')

  if settings.paypal_sandbox:
    return PP_SANDBOX_BASE_URL
  else:
    return PP_BASE_URL

@frappe.whitelist(allow_guest=True)
def process_pp(o, ppo, a):
  order = frappe.get_doc('Book Order', o)

  if not order.paid:
    pp_order = requests.get(pp_base_url() + '/v2/checkout/orders/' + ppo, headers={
      'Authorization': 'Bearer ' + get_paypal_token()
    }).json()
    
    if pp_order.get('status') == 'COMPLETED':
      r = requests.post(pp_base_url() + '/v2/payments/authorizations/' + a + '/capture', headers={
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + get_paypal_token(),
        'PayPal-Request-Id': frappe.generate_hash(),
      }).json()

      if r.get('message'):
        frappe.throw(r.get('message'))

      order.paypal_order_id = ppo
      order.paid = True
      order.save(ignore_permissions=True)
      frappe.db.commit()

  if order.paid:
    frappe.response.type = 'redirect'
    frappe.response.location = '/thank-you/' + order.name
    return

  return 'Error, please email admin@conjugaldictatorship.com'

def get_paypal_token():
  settings = frappe.get_doc('Book Settings')
  r = requests.post(pp_base_url() + '/v1/oauth2/token', data={
    'grant_type': 'client_credentials'
  }, headers={
    'Accept-Language': 'en_US',
  }, auth=(settings.paypal_client_id, settings.paypal_client_secret)).json()

  if r.get('message'):
    frappe.throw(r.get('message'))

  return r.get('access_token')

@frappe.whitelist()
def export_orders():
  book_orders = frappe.db.get_all('Book Order', filters={
    'paid': True,
    'queued_for_shipping': False,
  }, pluck='name')
  rows = []

  for name in book_orders:
    book_order = frappe.get_doc('Book Order', name)

    for item in book_order.items:
      rows.append([
        str(book_order.first_name) + ' ' + str(book_order.last_name),
        book_order.phone_number,
        book_order.email,
        book_order.name,
        str(book_order.street_address) + '\n' +
        (str(book_order.barangay) or (str(book_order.city) + ', ' + str(book_order.province))) + ' ' + str(book_order.postal_code) + '\n' +
        str(book_order.country),
        item.quantity,
        item.type,
        book_order.notes
      ])

    book_order.queued_for_shipping = True
    book_order.save()

  frappe.db.commit()
  return csvutils.build_csv_response(rows, 'cd_orders_export')

@frappe.whitelist()
def verify_paypal_amounts():
  orders_requiring_verification = frappe.db.get_all('Book Order', filters=[
    ['paypal_verified', '=', 0],
    ['payment_method', '=', 'PayPal / Credit Card'],
    ['paypal_order_id', 'is', 'set'],
    ['paid', '=', 1]
  ], fields=['name', 'paypal_order_id'])
  auth = 'Bearer ' + get_paypal_token()

  for order in orders_requiring_verification:
    pp_order = requests.get(pp_base_url() + '/v2/checkout/orders/' + order.paypal_order_id, headers={
      'Authorization': auth
    }).json()

    if pp_order.get('message'):
      print('Error checking order ' + order.name + ': ' + pp_order.get('message'))
      frappe.log_error(frappe.get_traceback(), pp_order.get('message'))

      if pp_order.get('message') == 'The specified resource does not exist.':
        frappe.db.set_value('Book Order', order.name, 'paypal_verified', True)
    else:
      owed = 0
  
      if pp_order.get('purchase_units'):
        for purchase_unit in pp_order.get('purchase_units'):
          if purchase_unit.get('payments'):
            payments = purchase_unit.get('payments')

            if payments.get('captures'):
              captures = payments.get('captures')

              for capture in captures:
                if capture.get('seller_receivable_breakdown') and capture.get('seller_receivable_breakdown').get('net_amount'):
                  owed += float(capture.get('seller_receivable_breakdown').get('net_amount').get('value'))

      if owed > 0:
        order = frappe.get_doc('Book Order', order.name)
        order.owed_by_jc = owed
        order.paypal_verified = True
        order.save(ignore_permissions=True)

  frappe.db.commit()
  return orders_requiring_verification

@frappe.whitelist(allow_guest=True)
def fifth_express(tracking_number):
  response = Response()

  response.content_type = 'text/html'
  response.response = '''
<form method="POST" action="https://fifthexpress.com/tracking" id="form">
  <input type="hidden" name="_method" value="POST" />
  <input type="hidden" name="data[Track][tracking_number]" value="''' + tracking_number + '''" />
</form>
Please wait...
<script>document.getElementById("form").submit();</script>
  '''

  return response

@frappe.whitelist(allow_guest=True)
def subscribe(subscriber):
  subscriber = json.loads(subscriber)
  new_subscriber = frappe.new_doc('Subscriber')
  
  for field in subscribe_fields:
    if field == 'date_of_birth':
      new_subscriber.set(field, utils.get_datetime(subscriber.get(field)))
    else:
      new_subscriber.set(field, subscriber.get(field))

  new_subscriber.save(ignore_permissions=True)
