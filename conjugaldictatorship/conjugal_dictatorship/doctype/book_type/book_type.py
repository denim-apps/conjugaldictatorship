# Copyright (c) 2022, JC Gurango and contributors
# For license information, please see license.txt

# import frappe
from frappe.model.document import Document

class BookType(Document):
	def db_update(self):
		self.name = self.label
		return super().db_update()
