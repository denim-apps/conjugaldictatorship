# Copyright (c) 2022, JC Gurango and contributors
# For license information, please see license.txt

import base64
import frappe
from frappe.model.document import Document
from frappe.email.doctype.email_template.email_template import get_email_template

class BookOrder(Document):
	def calculate(self):
		book_settings = frappe.get_doc('Book Settings')

		subtotal = 0
		discount = 0
		shipping = 0
		total_quantity = 0
		self_pickup = False

		for item in self.items:
			total_quantity += item.quantity
			subtotal += item.quantity * frappe.get_doc('Book Type', item.type).price
			item_discount_percentage = 0

			for bulk_discount in book_settings.discounts:
				if item.quantity >= bulk_discount.minimum_quantity:
					item_discount_percentage = max(bulk_discount.discount, item_discount_percentage)

			discount += item_discount_percentage / 100 * subtotal

			if frappe.get_doc('Book Type', item.type).pickup_only:
				self_pickup = True

		if self.country and not self_pickup:
			shipping = 0

			if self.country == 'Philippines':
				if self.barangay:
					if ', NCR' in self.barangay:
						if total_quantity > 4:
							shipping = 280
						elif total_quantity > 2:
							shipping = 140
						elif total_quantity > 1:
							shipping = 120
						else:
							shipping = 100
					else:
						if total_quantity > 4:
							shipping = 540
						elif total_quantity > 2:
							shipping = 270
						elif total_quantity > 1:
							shipping = 220
						else:
							shipping = 180
				else:
					shipping = 0
			else:
				shipping = 2500
		else:
			shipping = 0

		total = subtotal - discount + shipping

		self.subtotal = subtotal
		self.discount = discount
		self.shipping = shipping
		self.total = total

		if self.email and self.create_an_account == True and self.email != self.subscriber and frappe.db.exists('Subscriber', self.email):
			frappe.throw('An account with that email already exists')

	def db_insert(self):
		if not self.placed:
			self.calculate()

		return super().db_insert()

	def db_update(self):
		if not self.placed:
			self.calculate()

		if self.paid and not frappe.db.get_value('Book Order', { 'name': self.name }, fieldname='paid'):
			# Newly paid, send out the email.
			valid_dict = self.get_valid_dict()
			valid_dict['items'] = []

			for item in self.items:
				valid_dict['items'].append(item.get_valid_dict())

			email = get_email_template('Order Confirmation', valid_dict)
			frappe.sendmail(
				recipients = [self.email],
				cc=['jc@jcgurango.com'],
				subject = email['subject'],
				message = email['message'],
			)

			#self.owed_by_jc = self.total

		return super().db_update()

	def get_shipping_tracker(self):
		if self.tracking_number:
			if self.shipping_type == 'LBC':
				return 'https://lbcexpress.com/track/' + base64.b64encode((self.tracking_number + 'hashlbcexpress').encode('ascii')).decode('ascii')
			
			if self.shipping_type == 'GoGo Xpress':
				return 'https://app.gogoxpress.com/track/' + self.tracking_number

			if self.shipping_type == 'Fifth Express':
				return frappe.utils.get_url('/api/method/conjugaldictatorship.conjugal_dictatorship.fifth_express?tracking_number=' + self.tracking_number)

		return None
