// Copyright (c) 2022, JC Gurango and contributors
// For license information, please see license.txt

frappe.ui.form.on('Book Order', {
	refresh: function(frm) {
		const order = frm.doc;

		$('#order_details').html(`
			Order for:<br />
			${order.items.map((item) => {
				return `<b>${item.quantity} ${item.type}</b><br />`;
			})}
			<br />
			${order.first_name} ${order.last_name}<br />
			${order.phone_number}<br />
			${order.email}<br />
			${order.street_address}<br />
			${order.barangay || `${order.city}, ${order.province}`} ${order.postal_code || ''}<br />
			${order.country}
		`);
	}
});
