# Copyright (c) 2021, JC Gurango and contributors
# For license information, please see license.txt

from re import template
import frappe
from frappe import _
from frappe.email.doctype.email_template.email_template import get_email_template
from frappe.model.document import Document

class Subscriber(Document):
	def show_unique_validation_message(self, e):
		if frappe.db.db_type != 'postgres':
			fieldname = str(e).split("'")[-2]
			label = None

			# MariaDB gives key_name in error. Extracting fieldname from key name
			try:
				fieldname = self.get_field_name_by_key_name(fieldname)
			except IndexError:
				pass

			label = self.get_label_from_fieldname(fieldname)

			if fieldname == 'email':
				frappe.msgprint(_("This e-mail address is already in use for a subscription to the Conjugal Dictatorship."))
			else:
				frappe.msgprint(_("{0} must be unique").format(label or fieldname))

		# this is used to preserve traceback
		raise frappe.UniqueValidationError(self.doctype, self.name, e)

	def before_insert(self):
		self.activation_key = frappe.generate_hash(self.name)

	def after_insert(self):
		if not self.activated:
			email = get_email_template('Activate Subscription', self.get_valid_dict())
			frappe.sendmail(
				recipients = [self.email],
				subject = email['subject'],
				message = email['message'],
			)
