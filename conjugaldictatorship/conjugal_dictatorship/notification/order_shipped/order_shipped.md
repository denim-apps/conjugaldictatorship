<p>Hello {{ doc.first_name }} {{ doc.last_name }},</p>
<p>
    We've shipped out your order #{{ doc.name }} via {{ doc.shipping_type }}!
    {% if doc.tracking_number %}
    Your tracking number is <b>{{ doc.tracking_number }}</b>{% if doc.get_shipping_tracker() %}
    and you can track your shipment <a href="{{ doc.get_shipping_tracker() }}">here</a>{% endif %}.
    {% else %}
    You should have received a notification via your mobile number or email.
    {% endif %}
</p>
<p>Please note that for LBC shipments, it may take up to 24 hours to appear in the tracker.</p>
<p>Again, if you have any questions, feel free to respond to this email.</p>
<p>Thanks, JC</p>
