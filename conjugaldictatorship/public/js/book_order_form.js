jQuery(function ($) {
  const orderForm = $('#order_form');

  if (orderForm.length) {
    let selectedType = bookTypes[0];

    const form = frappe.ui.form.generate_website_form(orderForm, [
      {
        fieldname: 'binding',
        label: 'Choose Binding',
        fieldtype: 'Select',
        default: bookTypes[0].label,
        reqd: true,
        options: bookTypes.map(function (type) {
          return type.label;
        }),
        onchange: function(e) {
          if (e.target.value) {
            selectedType = bookTypes.find(function(type) {
              return type.label === e.target.value;
            });

            $('#book_image').attr('src', selectedType.image);
            calculate();
          }
        },
      },
      {
        fieldname: 'quantity',
        label: 'Quantity',
        fieldtype: 'Int',
        default: 1,
        reqd: true,
        onchange: function(e) {
          calculate();
        },
      },
      {
        fieldname: 'subtotal',
        label: 'Subtotal',
        fieldtype: 'HTML',
        options: '',
      }
    ]);

    form.addButton('Add to Cart', async function(form, e) {
      e.preventDefault();
      e.stopPropagation();
      const values = form.get_values();

      if (values) {
        $(form.wrapper).find('form')[0].submit();
      }
    });

    function calculate() {
      let total = '<table style="width: 100%;"><thead><tr><th>Product</th><th>Quantity</th><th>Unit Price</th></tr></thead>';
      
      total += '<tbody>';
      total += '<tr>';
      total += '<td>The Conjugal Dictatorship of Ferdinand and Imelda Marcos (' + selectedType.label + ')</td>';
      total += '<td>' + form.get_value('quantity') + '</td>';
      let price = '&#8369;' + format_number(selectedType.price, get_number_format('PHP'), 2);

      if (selectedType.srp) {
        price = '<strike>&#8369;' + format_number(selectedType.srp, get_number_format('PHP'), 2) + '</strike> <span class="text-success">' + price + "</span>";
      }

      total += '<td style="text-align: right;">' + price + '</td>';
      total += '</tr>';

      let discount = 0;
      
      bookDiscounts.forEach(function (definition) {
        if (form.get_value('quantity') >= definition.minimum_quantity) {
          discount = Math.max(definition.discount, discount);
        }
      });

      if (discount > 0) {
        total += '<tr>';
        total += '<td>Bulk Discount</td>';
        total += '<td>' + form.get_value('quantity') + '</td>';
        total += '<td style="text-align: right;">' + discount.toFixed(0) + '%</td>';
        total += '</tr>';
      }

      total += '</tbody>';
      total += '<tfoot>';
      total += '<tr>';
      total += '<td></td>';
      total += '<td><b>Total</b></td>';
      total += '<td style="text-align: right;">&#8369;' + format_number(selectedType.price * form.get_value('quantity') * (1 - (discount / 100)), get_number_format('PHP'), 2) + '</td>';
      total += '</tr>';

      if (selectedType.pickup_only) {
        total += '<tr><td colspan="3"><i>Note: For this quantity, you will have to arrange your own pickup. No shipping fee will be charged, and your order is subject to availability.</i></td></tr>';
      }

      total += '</tfoot>';

      total += '</table><br />';

      form.set_df_property('subtotal', 'options', total);
    }

    calculate();
    $(form.wrapper).find('form')
      .attr('method', 'POST')
      .attr('action', '/cart');
    $('[data-fieldname="binding"] label').css('font-weight', 'bold');
  }
});
