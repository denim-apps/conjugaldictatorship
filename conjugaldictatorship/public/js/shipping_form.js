jQuery(function ($) {
  const shippingForm = $('#shipping_form');

  if (shippingForm.length) {
    const form = frappe.ui.form.generate_website_form(shippingForm, [
      ...shippingFields.map((f) => ({
        ...f,
        reqd: !['create_an_account', 'new_password', 'city', 'province', 'postal_code'].includes(f.fieldname) || undefined,
        mandatory_depends_on: ['city', 'province'].includes(f.fieldname) ? 'eval:doc.country !== "Philippines"' : (['barangay'].includes(f.fieldname) ? 'eval:doc.country === "Philippines"' : undefined),
      })),
    ]);

    const recalculate = function () {
      let shippingType = 'ncr';

      if (form.get_value('country') === 'Philippines') {
        shippingType = form.get_value('barangay').indexOf(', NCR') > -1 ? 'ncr' : 'domestic';
        form.set_df_property('barangay', 'reqd', true);
        form.set_df_property('city', 'reqd', false);
        form.set_df_property('province', 'reqd', false);
      } else {
        shippingType = 'international';
        form.set_df_property('barangay', 'reqd', false);
        form.set_df_property('city', 'reqd', true);
        form.set_df_property('province', 'reqd', true);
      }

      let shippingCost = 0;

      if (!selfPickup) {
        if (shippingType === 'ncr') {
          if (totalQuantity > 4) {
            shippingCost = 280;
          } else if (totalQuantity > 2) {
            shippingCost = 140;
          } else if (totalQuantity > 1) {
            shippingCost = 120;
          } else {
            shippingCost = 100;
          }
        } else if (shippingType === 'domestic') {
          if (totalQuantity > 4) {
            shippingCost = 540;
          } else if (totalQuantity > 2) {
            shippingCost = 270;
          } else if (totalQuantity > 1) {
            shippingCost = 220;
          } else {
            shippingCost = 180;
          }
        } else {
          shippingCost = 2500;
        }
      }

      $('#shippingTotal').text('₱' + format_number(shippingCost, get_number_format('PHP'), 2));
      $('#totalTotal').text('₱' + format_number(totalWithoutShipping + shippingCost, get_number_format('PHP'), 2));
    };

    form.set_values(order);
    form.set_df_property('country', 'onchange', recalculate);
    form.set_df_property('barangay', 'onchange', recalculate);

    $('#checkoutButton').click(async function () {
      const values = form.get_values();

      if (values) {
        frappe.call({
          method: 'conjugaldictatorship.conjugal_dictatorship.checkout',
          args: values,
          btn: $('#checkoutButton'),
          callback: function (r) {
            if (!r.exc) {
              if (r.message) {
                window.location.href = r.message;
              }
            }
          }
        });
      }
    });

    if (isSubscribeForm) {
      form.addButton('Subscribe', async function(form, e) {
        e.preventDefault();
        e.stopPropagation();
        const values = form.get_values();
  
        if (values) {
          frappe.call({
            method: 'conjugaldictatorship.conjugal_dictatorship.subscribe',
            args: {
              subscriber: values,
            },
            callback: function(r) {
              if (!r.exc) {
                window.location.href = '/subscribed';
              }
            }
        });
        }
      }, 'primary');
      $(form.wrapper).find('form')
        .attr('method', 'POST')
        .attr('action', '')
        .append('<input type="hidden" name="subscribe" value="1" />');
    }
  }
});
