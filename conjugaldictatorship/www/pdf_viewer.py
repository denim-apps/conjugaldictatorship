import frappe
import random
import json
import base64
from frappe import utils

def get_context(context):
  block = frappe.get_doc('Web Page Block', frappe.local.form_dict['file'])
  file = frappe.get_doc('File', json.loads(block.web_template_values)['file'])
  all_characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='
  cypher_key0i = []
  cypher_key1i = []
  cypher_key0 = ''
  cypher_key1 = ''

  for i in range(65):
    cypher_key0i.append(i)
    cypher_key1i.append(i)

  random.shuffle(cypher_key0i)
  random.shuffle(cypher_key1i)

  for i in range(65):
    cypher_key0 += all_characters[cypher_key0i[i]]
    cypher_key1 += all_characters[cypher_key1i[i]]

  context['cypher'] = json.dumps([cypher_key0, cypher_key1])
  pdf_b64 = base64.b64encode(file.get_content()).decode('ascii')
  context['pdf_b64'] = ''

  for i in range(len(pdf_b64)):
    value = pdf_b64[i]
    context['pdf_b64'] += cypher_key1[cypher_key0.index(value)]

  context['pdf_b64'] = json.dumps(context['pdf_b64'])
  context['pdf_id'] = utils.random_string(12)
