import frappe

def get_context(context):
  subscriber = frappe.db.get_value('Subscriber', {
    'activation_key': frappe.form_dict['key'],
    'activated': False
  })

  if subscriber:
    subscriber = frappe.get_doc('Subscriber', subscriber)
    context.first_name = subscriber.first_name
    context.last_name = subscriber.last_name

    if frappe.form_dict.get('password') and frappe.form_dict.get('password2') and frappe.form_dict.get('password') == frappe.form_dict.get('password2'):
      # Create a user
      subscriber.activated = True
      subscriber.activation_key = None

      user = frappe.new_doc('User')
      user.email = subscriber.email
      user.first_name = subscriber.first_name
      user.last_name = subscriber.last_name
      user.new_password = frappe.form_dict.get('password')
      user.user_type = 'Website User'
      user.send_welcome_email = False
      user.save(ignore_permissions=True)

      subscriber.user = user.name
      subscriber.save(ignore_permissions=True)
      context.subscribed = True

    return context
  else:
    frappe.local.response.type = 'redirect'
    frappe.local.response.location = '/'
    return
