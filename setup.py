from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in conjugaldictatorship/__init__.py
from conjugaldictatorship import __version__ as version

setup(
	name="conjugaldictatorship",
	version=version,
	description="Conjugal dictatorship entities and modifications.",
	author="JC Gurango",
	author_email="jc@jcgurango.com",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
